package fr.soleil.comete.bean.tangotree.data;

import fr.soleil.comete.bean.tangotree.TangoTree;

/**
 * An {@link Enum} that represents at which level data can be considered as selected (i.e. sendable) in
 * {@link TangoTree}.
 * 
 * @author GIRARDOT
 *
 */
public enum DataLevel {
    DEVICE, ATTRIBUTE
}