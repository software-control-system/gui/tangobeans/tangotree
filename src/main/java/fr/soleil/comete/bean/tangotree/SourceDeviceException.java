package fr.soleil.comete.bean.tangotree;

public class SourceDeviceException extends Exception {

    private static final long serialVersionUID = 2951447187181413843L;

    public SourceDeviceException(final String message) {
        super(message);

    }

}
