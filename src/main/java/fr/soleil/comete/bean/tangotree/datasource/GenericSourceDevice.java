package fr.soleil.comete.bean.tangotree.datasource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.tango.utils.DevFailedUtils;
import org.tango.utils.TangoUtil;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.comete.bean.tangotree.SourceDeviceException;

public class GenericSourceDevice implements ISourceDevice {

    private final String deviceCommand;

    public GenericSourceDevice(final String deviceCommand) {
        this.deviceCommand = deviceCommand;
    }

    @Override
    public List<String> getSourceList() throws SourceDeviceException {
        List<String> sourceList = new ArrayList<String>();

        try {
            String deviceName = TangoUtil.getFullDeviceNameForCommand(deviceCommand);
            // System.out.println("deviceName = " + deviceName);
            String commandName = deviceCommand.replaceFirst(deviceName + DEFAULT_SEPARATOR, "");
            // System.out.println("commandName = " + commandName);
            if(deviceName != null) {
                DeviceProxy proxy = getDeviceProxy(deviceName);
                if (proxy != null) {
                    DeviceData result = proxy.command_inout(commandName);
                    if (result != null) {
                        String[] sourceArray = result.extractStringArray();
                        sourceList = Arrays.asList(sourceArray);
                    }
                }
            }
        } catch (DevFailed e) {
            throw new SourceDeviceException(DevFailedUtils.toString(e));
        }
        return sourceList;
    }

    public static DeviceProxy getDeviceProxy(final String deviceName) throws DevFailed {
        DeviceProxy proxy = null;
        if (deviceName != null) {
            proxy = DeviceProxyFactory.get(deviceName);
        }
        return proxy;
    }

    @Override
    public String getName() {
        return deviceCommand;
    }

    @Override
    public String getInformation() {
        return deviceCommand;
    }

    @Override
    public String getPathSeparator() {
        return DEFAULT_SEPARATOR;
    }

}
