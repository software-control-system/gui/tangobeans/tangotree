package fr.soleil.comete.bean.tangotree.util;

import javax.swing.tree.TreeNode;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.icons.Icons;

public class TangoTreeUtils {

    public static final MessageManager MESSAGE_MANAGER = new MessageManager("fr.soleil.comete.bean.tangotree");
    public static final Icons ICONS = new Icons("fr.soleil.comete.bean.tangotree.icons");
    public static final String DEFAULT_TANGO_HOST;
    static {
        String propName = "TANGO_HOST";
        String host = System.getProperty(propName);
        if (host == null) {
            host = System.getenv(propName);
            if (host == null) {
                host = ObjectUtils.EMPTY_STRING;
            }
        }
        DEFAULT_TANGO_HOST = host;
    }
    public static final String TANGO_SEPARATOR = "/";
    public final static String TANGO_WILDCARD = "*";

    private TangoTreeUtils() {
        // nothing to do: just hide constructor
    }

    /**
     * Return the depth (distance to root) of a {@link TreeNode}.
     * 
     * @param node The {@link TreeNode}.
     * @return An <code>int</code>. <code>-1</code> is <code>node</code> is <code>null</code>.
     */
    public static int getDepth(TreeNode node) {
        int depth = -1;
        TreeNode tmp = node;
        while (tmp != null) {
            depth++;
            tmp = tmp.getParent();
        }
        return depth;
    }

    public static Database getDatabase(String tangoHost) throws DevFailed {
        Database db;
        if (tangoHost == null) {
            db = null;
        } else if (DEFAULT_TANGO_HOST.equalsIgnoreCase(tangoHost)) {
            db = ApiUtil.get_db_obj();
        } else {
            db = ApiUtil.get_db_obj(tangoHost);
        }
        return db;
    }

    public static DeviceProxy getDeviceProxy(String tangoHost, String device) throws DevFailed {
        DeviceProxy proxy;
        if ((tangoHost == null) || tangoHost.isEmpty()) {
            proxy = null;
        } else if (DEFAULT_TANGO_HOST.equalsIgnoreCase(tangoHost)) {
            proxy = DeviceProxyFactory.get(device);
        } else {
            proxy = DeviceProxyFactory.get(device, tangoHost);
        }
        return proxy;
    }
}
