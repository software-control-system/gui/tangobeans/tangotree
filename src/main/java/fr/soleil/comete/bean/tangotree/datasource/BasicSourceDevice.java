package fr.soleil.comete.bean.tangotree.datasource;

import java.util.ArrayList;
import java.util.List;

public class BasicSourceDevice implements ISourceDevice {

    @Override
    public List<String> getSourceList() {
        List<String> list = new ArrayList<String>();
        list.add("toto/tata");
        list.add("toto/titi");
        list.add("tota/titi");
        return list;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "Test";
    }

    @Override
    public String getInformation() {
        // TODO Auto-generated method stub
        return "Test source";
    }

    @Override
    public String getPathSeparator() {
        return DEFAULT_SEPARATOR;
    }

}
