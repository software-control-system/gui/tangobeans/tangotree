package fr.soleil.comete.bean.tangotree.datasource;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceData;
import fr.soleil.comete.bean.tangotree.SourceDeviceException;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.SystemUtils;

public class DatabaseTangoSourceDevice implements ISourceDevice {

    private static final String JOKER = "*";
    private static final String[] FORBIDDEN_CLASSES = { "DataBase", "Starter", "TangoAccessControl", "DServer" };

    private final List<String> forbiddenDevicesList;
    private final boolean onlyExportedDevices;
    private final boolean withoutForbiddenDevices;

    public DatabaseTangoSourceDevice() {
        this(true);
    }

    public DatabaseTangoSourceDevice(final boolean withoutForbiddenDevicesList) {
        this(withoutForbiddenDevicesList, false);
    }

    public DatabaseTangoSourceDevice(final boolean withoutForbiddenDevicesList, final boolean onlyExportedDevices) {
        this.onlyExportedDevices = onlyExportedDevices;
        this.withoutForbiddenDevices = withoutForbiddenDevicesList;
        forbiddenDevicesList = new LinkedList<String>();
    }

    private void fillForbiddenDevicesList() throws SourceDeviceException {
        try {
            Database database = TangoDeviceHelper.getDatabase();

            for (String className : FORBIDDEN_CLASSES) {
                DeviceData deviceData = new DeviceData();

                deviceData.insert(className);
                database.set_timeout_millis(1000);

                String[] classDevices = database.get_device_exported_for_class(className);

                for (String device : classDevices) {
                    forbiddenDevicesList.add(device);

                }

            }
        } catch (DevFailed e) {
            throw new SourceDeviceException(DevFailedUtils.toString(e));
        }
    }

    @Override
    public List<String> getSourceList() throws SourceDeviceException {

        List<String> devicesList = new LinkedList<String>();
        List<String> domaineList = new ArrayList<String>();
        List<String> familyList = new ArrayList<String>();
        List<String> memberList = new ArrayList<String>();
        String pathSeparator = getPathSeparator();

        String[] deviceArray = null;
        try {
            if (forbiddenDevicesList.isEmpty() && withoutForbiddenDevices) {
                fillForbiddenDevicesList();
            }

            Database database = TangoDeviceHelper.getDatabase();
            DeviceData deviceData = new DeviceData();

            deviceData.insert(JOKER);
            database.set_timeout_millis(10000);
            if (onlyExportedDevices) {
                deviceArray = database.get_device_exported(JOKER);
            } else {
                deviceArray = database.get_device_list(JOKER);
            }

        } catch (DevFailed e) {
            throw new SourceDeviceException(DevFailedUtils.toString(e));
        }

        if (deviceArray != null) {
            String strFamily = null;
            String strDomain = null;
            String strMember = null;

            String containsKeyDomaine = null;
            String containsKeyFamily = null;
            String containsKeyMember = null;

            String deviceName = null;

            String[] split = null;
            for (String device : deviceArray) {

                boolean isForbidden = false;
                for (String forbiddenDevice : forbiddenDevicesList) {
                    if (device.equals(forbiddenDevice)) {
                        isForbidden = true;
                    }
                }

                if (!isForbidden) {
                    // Group the device ignoring the case see JIRA JAVAAPI-356
                    split = device.split(pathSeparator);

                    if (split != null) {
                        if (split.length > 0) {
                            strDomain = split[0];
                            containsKeyDomaine = containsKey(strDomain, domaineList);
                            if (containsKeyDomaine == null) {
                                containsKeyDomaine = strDomain;
                                domaineList.add(strDomain);
                            }

                            if (split.length > 1) {
                                strFamily = split[1];
                                containsKeyFamily = containsKey(strFamily, familyList);
                                if (containsKeyFamily == null) {
                                    containsKeyFamily = strFamily;
                                    familyList.add(strFamily);
                                }
                            }

                            if (split.length > 2) {
                                strMember = split[2];
                                containsKeyMember = containsKey(strMember, memberList);
                                if (containsKeyMember == null) {
                                    containsKeyMember = strMember;
                                    memberList.add(strMember);
                                }
                            }
                        }

                        deviceName = containsKeyDomaine + pathSeparator + containsKeyFamily + pathSeparator
                                + containsKeyMember;
                        if (deviceName != null) {
                            devicesList.add(deviceName);
                        }
                    }
                }
            }
        }
        return devicesList;
    }

    private String containsKey(String testKey, List<String> stringList) {
        String key = null;
        if (testKey != null && stringList != null) {
            for (String compareString : stringList) {
                if (testKey.equalsIgnoreCase(compareString)) {
                    key = compareString;
                    break;
                }
            }
        }
        return key;
    }

    @Override
    public String getName() {

        return SystemUtils.getSystemProperty("TANGO_HOST");

    }

    @Override
    public String getInformation() {

        return "Tango Database";
    }

    @Override
    public String getPathSeparator() {
        return DEFAULT_SEPARATOR;
    }

}
