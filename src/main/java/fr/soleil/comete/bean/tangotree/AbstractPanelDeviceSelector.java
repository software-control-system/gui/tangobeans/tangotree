package fr.soleil.comete.bean.tangotree;

import java.net.URL;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.bean.tangotree.util.TangoTreeUtils;
import fr.soleil.comete.definition.event.TreeNodeSelectionEvent;
import fr.soleil.comete.definition.listener.IButtonListener;
import fr.soleil.comete.definition.listener.ITreeNodeSelectionListener;
import fr.soleil.comete.definition.widget.IButton;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.IScrollPane;
import fr.soleil.comete.definition.widget.ITextField;
import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.CometeWidgetFactory;
import fr.soleil.comete.definition.widget.util.CometeWidgetFactory.CometePackage;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.lib.project.ObjectUtils;

public abstract class AbstractPanelDeviceSelector
        implements IDeviceSelector, IButtonListener, ITreeNodeSelectionListener {

    private static final String EXPAND_TEXT_KEY = "fr.soleil.comete.bean.tangotree.expand.text";
    private static final String EXPAND_TOOLTIP_KEY = "fr.soleil.comete.bean.tangotree.expand.tooltip";
    private static final String COLLAPSE_TEXT_KEY = "fr.soleil.comete.bean.tangotree.collapse.text";
    private static final String COLLAPSE_TOOLTIP_KEY = "fr.soleil.comete.bean.tangotree.collapse.tooltip";
    private static final String MATCH_KEY = "fr.soleil.comete.bean.tangotree.match";
    private static final String SLASH = "/";
    private static final String STAR = "*";
    private static final String ESCAPED_STAR = "\\*";
    private static final String POINT_STAR = ".*";

    private final List<IDeviceSelectorListener> listeners = new ArrayList<>();
    private IComponent selectorComponent;
    private IScrollPane scrollPane;
    private ITree tree;
    private ITextField expressionText;
    private final Object parent;

    private ISourceDevice sourceDevice;
    private CometeImage rootImage;
    private CometeImage nodeImage;
    private CometeImage leafImage;

    private boolean multiSelection = true;
    private static CometeImage errorImage = null;
    private ITreeNode currentRootNode = null;
    private boolean sourceInError = false;

    private boolean filter = false;

    public AbstractPanelDeviceSelector(final CometePackage cometePackage) {
        this(cometePackage, null, null, null, null, false);
    }

    public AbstractPanelDeviceSelector(final CometePackage cometePackage, final ISourceDevice sourceDevice) {
        this(cometePackage, sourceDevice, null, null, null, false);
    }

    public AbstractPanelDeviceSelector(final CometePackage cometePackage, final ISourceDevice sourceDevice,
            final CometeImage rootImage, final CometeImage nodeImage, final CometeImage leafImage,
            final boolean multiselection) {
        this(cometePackage, null, sourceDevice, rootImage, nodeImage, leafImage, multiselection);
    }

    public AbstractPanelDeviceSelector(final CometePackage cometePackage, final Object parent,
            final ISourceDevice sourceDevice, final CometeImage rootImage, final CometeImage nodeImage,
            final CometeImage leafImage, final boolean multiselection) {
        CometeWidgetFactory.setCurrentPackage(cometePackage);
        this.parent = parent;
        this.sourceDevice = sourceDevice;
        setRootImage(rootImage);
        setLeafImage(leafImage);
        setNodeImage(nodeImage);
        setMultiSelection(multiselection);
        getSelectorComponent();
        URL url = this.getClass().getResource("/com/famfamfam/silk/cancel.png");
        if (url != null) {
            errorImage = new CometeImage(url.toString());
        }
    }

    @Override
    public IComponent getSelectorComponent() {
        if (selectorComponent == null) {
            selectorComponent = CometeWidgetFactory.createPanel(parent, 0);
            if (selectorComponent instanceof IPanel) {
                ((IPanel) selectorComponent).setLayout(getComponentLayout());
                scrollPane = CometeWidgetFactory.createScrollPane(selectorComponent, 0);
                // Create tree
                getTree();

                // Add scrollPanel
                if (scrollPane != null) {
                    ((IPanel) selectorComponent).add(scrollPane, getTreeConstraints());
                }

                // Create search device panel
                IPanel searchPanel = CometeWidgetFactory.createPanel(selectorComponent, 0);
                if (searchPanel != null) {
                    searchPanel.setLayout(getSearchPanelLayout());

                    // Create expendButton panel
                    IPanel expendButtonPanel = CometeWidgetFactory.createPanel(searchPanel, 0);
                    if (expendButtonPanel != null) {
                        searchPanel.add(expendButtonPanel, getExpendButtonsConstraints());
                        IButton expendAllButton = CometeWidgetFactory.createButton(expendButtonPanel, 0);
                        expendAllButton.setText(TangoTreeUtils.MESSAGE_MANAGER.getMessage(EXPAND_TEXT_KEY));
                        expendAllButton.setToolTipText(TangoTreeUtils.MESSAGE_MANAGER.getMessage(EXPAND_TOOLTIP_KEY));
                        expendButtonPanel.add(expendAllButton);
                        expendAllButton.addButtonListener(new IButtonListener() {
                            @Override
                            public void actionPerformed(EventObject event) {
                                expendAll(true);
                            }
                        });

                        IButton collapseAllButton = CometeWidgetFactory.createButton(expendButtonPanel, 0);
                        collapseAllButton.setText(TangoTreeUtils.MESSAGE_MANAGER.getMessage(COLLAPSE_TEXT_KEY));
                        collapseAllButton
                                .setToolTipText(TangoTreeUtils.MESSAGE_MANAGER.getMessage(COLLAPSE_TOOLTIP_KEY));
                        expendButtonPanel.add(collapseAllButton);
                        collapseAllButton.addButtonListener(new IButtonListener() {
                            @Override
                            public void actionPerformed(EventObject event) {
                                expendAll(false);
                            }
                        });

                    }

                    // Create search field
                    expressionText = CometeWidgetFactory.createTextField(searchPanel, 0);
                    if (expressionText != null) {
                        searchPanel.add(expressionText, getSearchFieldConstraints());
                    }

                    // Create button
                    IButton matchButton = CometeWidgetFactory.createButton(searchPanel, 0);
                    if (matchButton != null) {
                        matchButton.setText(TangoTreeUtils.MESSAGE_MANAGER.getMessage(MATCH_KEY));
                        matchButton.addButtonListener(this);
                        searchPanel.add(matchButton, getSearchButtonConstraints());
                    }

                    // Add search panel
                    ((IPanel) selectorComponent).add(searchPanel, getSearchPanelConstraints());
                }
            }

            fillTree();
            setRootImage(rootImage);
            setLeafImage(leafImage);
            setNodeImage(nodeImage);
        }
        return selectorComponent;
    }

    public ITree getTree() {
        if ((tree == null) && (scrollPane != null)) {
            int selectionType = ITree.SINGLE_SELECTION;
            if (isMultiSelection()) {
                selectionType = ITree.MULTI_SELECTION;
            }
            tree = CometeWidgetFactory.createTree(scrollPane, selectionType | 0);
            if (tree != null) {
                tree.setSelectionMode(selectionType);
                scrollPane.setView(tree);

                tree.addTreeNodeSelectionListener(this);
            }
        }
        return tree;
    }

    public boolean isMultiSelection() {
        return multiSelection;
    }

    public void setMultiSelection(final boolean multiSelection) {
        this.multiSelection = multiSelection;
        if (tree != null) {
            int selectionType = ITree.SINGLE_SELECTION;
            if (isMultiSelection()) {
                selectionType = ITree.MULTI_SELECTION;
            }
            tree.setSelectionMode(selectionType);
        }
    }

    public abstract void expendAll(boolean expend);

    public abstract void refreshTree();

    protected abstract Object getComponentLayout();

    protected abstract Object getTreeConstraints();

    protected abstract Object getSearchPanelLayout();

    protected abstract Object getSearchPanelConstraints();

    protected abstract Object getSearchFieldConstraints();

    protected abstract Object getSearchButtonConstraints();

    protected abstract Object getExpendButtonsConstraints();

    @Override
    public ISourceDevice getSourceDevice() {
        return sourceDevice;
    }

    @Override
    public void setSourceDevice(final ISourceDevice sourceDevice) {
        this.sourceDevice = sourceDevice;
        if (expressionText != null) {
            expressionText.setText(ObjectUtils.EMPTY_STRING);
        }
        clearSelectedDevices();
        fillTree();
    }

    public CometeImage getRootImage() {
        return rootImage;
    }

    public void setRootImage(final CometeImage image) {
        this.rootImage = image;
        if (tree != null) {
            ITreeNode rootNode = tree.getRootNode();
            if (rootNode != null) {
                CometeImage tmpRootImage = rootImage;
                if (sourceInError) {
                    tmpRootImage = errorImage;
                }
                rootNode.setImage(tmpRootImage);
                refreshTree();
            }
        }
    }

    public CometeImage getNodeImage() {
        return nodeImage;
    }

    public void setNodeImage(final CometeImage image) {
        this.nodeImage = image;
        refreshNodeAndLeafImage();
    }

    private void refreshNodeAndLeafImage() {
        if (tree != null) {
            ITreeNode rootNode = tree.getRootNode();
            if (rootNode != null) {
                List<ITreeNode> children = rootNode.getChildren();
                refreshNodeAndLeafImage(children);
            }
        }
        refreshTree();
    }

    private void refreshNodeAndLeafImage(final List<ITreeNode> children) {
        if (children != null) {
            for (ITreeNode child : children) {
                List<ITreeNode> children2 = child.getChildren();
                if ((children2 == null) || children2.isEmpty()) {
                    child.setImage(leafImage);
                } else {
                    child.setImage(nodeImage);
                }
                refreshNodeAndLeafImage(children2);
            }
        }
    }

    public CometeImage getLeafImage() {
        return leafImage;
    }

    public void setLeafImage(final CometeImage image) {
        this.leafImage = image;
        refreshNodeAndLeafImage();
    }

    @Override
    public List<String> getSelectedDevices() {
        // return only the leaf selection
        List<String> selectedDevice = null;
        if (tree != null) {
            ITreeNode[] nodes = tree.getSelectedNodes();
            if (nodes != null) {
                selectedDevice = new ArrayList<>();
                List<ITreeNode> children = null;
                for (ITreeNode node : nodes) {
                    children = node.getChildren();
                    if (((children == null) || children.isEmpty()) && (node.getData() != null)) {
                        selectedDevice.add(node.getData().toString());
                    }
                }
            }
        }
        return selectedDevice;
    }

    @Override
    public void addDeviceSelectorListener(final IDeviceSelectorListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeDeviceSelectorListener(final IDeviceSelectorListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }

    @Override
    public void selectionChanged(final TreeNodeSelectionEvent event) {
        List<String> selectedDevices = getSelectedDevices();
        for (IDeviceSelectorListener listener : listeners) {
            listener.selectionChanged(selectedDevices);
        }
    }

    @Override
    public void clearSelectedDevices() {
        if (tree != null) {
            tree.clearSelection();
        }
        filter = false;
    }

    private void fillTree() {
        String pathSeparator = SLASH;
        if (sourceDevice != null) {
            pathSeparator = sourceDevice.getPathSeparator();
        }
        List<String> deviceList = null;
        sourceInError = false;
        try {
            deviceList = match();
        } catch (Exception e) {
            sourceInError = true;
            currentRootNode = new BasicTreeNode();
            currentRootNode.setName(sourceDevice.getName());
            currentRootNode.setImage(errorImage);
            currentRootNode.setToolTip(e.getMessage());
            tree.setRootNode(currentRootNode);
            refreshTree();
        }
        if ((!sourceInError) && (deviceList != null) && (tree != null)) {
            ITreeNode rootNode = new BasicTreeNode();

            for (String path : deviceList) {
                String[] pathSplit = path.split(pathSeparator);
                ITreeNode currentNode = rootNode;
                String nodePath = null;
                String el = null;
                for (String element : pathSplit) {
                    el = element;
                    ITreeNode node = null;
                    for (ITreeNode child : currentNode.getChildren()) {
                        if (child.getName().equals(el)) {
                            node = child;
                        }
                    }
                    if (node == null) {
                        node = new BasicTreeNode();
                    }
                    node.setName(el);
                    ITreeNode[] childToAdd = new ITreeNode[1];
                    childToAdd[0] = node;
                    currentNode.addNodes(childToAdd);

                    if (nodePath == null) {
                        nodePath = el;
                    } else {
                        nodePath = nodePath + pathSeparator + el;
                    }
                    node.setData(nodePath);
                    currentNode = node;
                }
            }

            rootNode.setName(sourceDevice.getName());
            rootNode.setToolTip(sourceDevice.getInformation());
            tree.setRootNode(rootNode);
            // System.out.println("tooltip=" + rootNode.getToolTip());
        }
    }

    @Override
    public void actionPerformed(final EventObject event) {
        filter = true;
        fillTree();
    }

    private List<String> match() throws SourceDeviceException {
        List<String> matchedDeviceList = null;
        if (sourceDevice != null) {
            matchedDeviceList = sourceDevice.getSourceList();
            if (filter && (expressionText != null) && (matchedDeviceList != null)) {
                String expression = expressionText.getText();
                if ((expression != null) && !expression.trim().isEmpty()) {
                    if (!expression.contains(STAR)) {
                        expression = STAR + expression + STAR;
                    }
                    expression = expression.replaceAll(ESCAPED_STAR, POINT_STAR);
                    List<String> initialSourceDeviceList = matchedDeviceList;
                    matchedDeviceList = new ArrayList<>();
                    Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
                    for (String device : initialSourceDeviceList) {
                        Matcher matcher = pattern.matcher(device);
                        if (matcher.matches()) {
                            matchedDeviceList.add(device);
                        }
                    }
                }
            }
        }
        return matchedDeviceList;
    }

}
