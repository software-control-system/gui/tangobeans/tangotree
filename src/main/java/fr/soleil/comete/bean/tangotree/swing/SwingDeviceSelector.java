package fr.soleil.comete.bean.tangotree.swing;

import java.awt.BorderLayout;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.soleil.comete.bean.tangotree.AbstractPanelDeviceSelector;
import fr.soleil.comete.bean.tangotree.IDeviceSelectorListener;
import fr.soleil.comete.bean.tangotree.datasource.ArchivingSourceDevice;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.CometeWidgetFactory;
import fr.soleil.comete.swing.Tree;

public class SwingDeviceSelector extends AbstractPanelDeviceSelector {

    private boolean rootVisible = false;

    public SwingDeviceSelector() {
        super(CometeWidgetFactory.CometePackage.SWING);
    }

    public SwingDeviceSelector(final ISourceDevice sourceDevice) {
        super(CometeWidgetFactory.CometePackage.SWING, sourceDevice);
    }

    public SwingDeviceSelector(final ISourceDevice sourceDevice, final boolean rootVisible,
            final boolean multiSelection) {
        super(CometeWidgetFactory.CometePackage.SWING, sourceDevice, null, null, null, multiSelection);
        setRootVisible(rootVisible);
    }

    public SwingDeviceSelector(final ISourceDevice sourceDevice, final boolean rootVisible, final CometeImage rootImage,
            final CometeImage nodeImage, final CometeImage leafImage, final boolean multiSelection) {
        super(CometeWidgetFactory.CometePackage.SWING, sourceDevice, rootImage, nodeImage, leafImage, multiSelection);
        setRootVisible(rootVisible);
    }

    public boolean isRootVisible() {
        return rootVisible;
    }

    public void setRootVisible(final boolean isRootVisible) {
        this.rootVisible = isRootVisible;
        ITree tree = getTree();
        if (tree instanceof Tree) {
            Tree cometeTree = (Tree) tree;
            cometeTree.setRootVisible(isRootVisible);
        }
    }

    @Override
    public void refreshTree() {
        ITree tree = getTree();
        if (tree instanceof Tree) {
            Tree cometeTree = (Tree) tree;
            cometeTree.repaint();
        }
    }

    @Override
    protected Object getComponentLayout() {
        return new BorderLayout();
    }

    @Override
    protected Object getTreeConstraints() {
        return BorderLayout.CENTER;
    }

    @Override
    protected Object getSearchPanelLayout() {
        return new BorderLayout();
    }

    @Override
    protected Object getSearchFieldConstraints() {
        return BorderLayout.CENTER;
    }

    @Override
    protected Object getSearchButtonConstraints() {
        return BorderLayout.EAST;
    }

    @Override
    protected Object getSearchPanelConstraints() {
        return BorderLayout.SOUTH;
    }

    @Override
    protected Object getExpendButtonsConstraints() {
        return BorderLayout.WEST;
    }

    @Override
    public void expendAll(boolean expend) {
        ITree tree = getTree();
        if (tree instanceof Tree) {
            Tree cometeTree = (Tree) tree;
            cometeTree.expandAll(expend);
        }
    }

    /**
     * Demo
     * 
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        URL file = frame.getClass().getResource("/com/famfamfam/silk/book.png");
        CometeImage img = new CometeImage(file.toString());

        ISourceDevice archivingSource = new ArchivingSourceDevice();
        final SwingDeviceSelector swingSelector = new SwingDeviceSelector(archivingSource, false, null, img, null,
                false);
        swingSelector.setMultiSelection(false);
        IComponent selectorComponent = swingSelector.getSelectorComponent();

        swingSelector.addDeviceSelectorListener(new IDeviceSelectorListener() {
            @Override
            public void selectionChanged(final List<String> selectedDevices) {
                if ((selectedDevices != null) && !selectedDevices.isEmpty()) {
                    System.out.println("Selected =" + Arrays.toString(selectedDevices.toArray()));
                } else {
                    System.out.println("Selection is empty");
                }
            }
        });

        frame.add((JComponent) selectorComponent);
        frame.setSize(1600, 800);
        frame.setVisible(true);

    }

}
