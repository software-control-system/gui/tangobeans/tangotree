package fr.soleil.comete.bean.tangotree.data;

/**
 * An {@link Enum} that represents the type of data one can expect to find in a tango attribute.
 * 
 * @author GIRARDOT
 */
public enum DataType {
    BOOLEAN, NUMBER, STATE, STRING
}