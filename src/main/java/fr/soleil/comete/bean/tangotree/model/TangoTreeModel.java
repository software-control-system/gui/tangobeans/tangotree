package fr.soleil.comete.bean.tangotree.model;

import java.lang.ref.WeakReference;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import fr.esrf.TangoApi.Database;
import fr.soleil.comete.bean.tangotree.TangoTree;
import fr.soleil.comete.bean.tangotree.node.TangoNode;
import fr.soleil.comete.bean.tangotree.util.TangoTreeUtils;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;

/**
 * The {@link DefaultTreeModel} used by {@link TangoTree}
 * 
 * @author GIRARDOT
 */
public class TangoTreeModel extends DefaultTreeModel {

    private static final long serialVersionUID = 3358385390856565092L;

    protected final String tangoHost;
    protected String filteredDeviceClass;
    protected volatile TreeStructureWorker lastStructureWorker;

    /**
     * Creates a new {@link TangoTreeModel} connected to default TANGO_HOST.
     */
    public TangoTreeModel() {
        this(TangoTreeUtils.DEFAULT_TANGO_HOST);
    }

    /**
     * Creates a new {@link TangoTreeModel} connected to desired TANGO_HOST.
     * 
     * @param tangoHost The desired TANGO_HOST.
     */
    public TangoTreeModel(String tangoHost) {
        this(new TangoNode(tangoHost), false);
    }

    /**
     * Creates a new {@link TangoTreeModel} with given root {@link TangoNode}, specifying whether any node can have
     * children, or whether only certain nodes can have children.
     * 
     * @param root The root {@link TangoNode}.
     * @param asksAllowsChildren a <code>boolean</code>, <code>false</code> if any node can have children,
     *            <code>true</code> if each node is asked to see if it can have children.
     */
    public TangoTreeModel(TangoNode root, boolean asksAllowsChildren) {
        super(root, asksAllowsChildren);
        this.tangoHost = root == null ? null : root.getFullPath();
        this.filteredDeviceClass = null;
    }

    @Override
    public TangoNode getRoot() {
        return (TangoNode) super.getRoot();
    }

    @Override
    public void setRoot(TreeNode root) {
        if (root instanceof TangoNode) {
            super.setRoot(root);
        }
    }

    /**
     * Returns the filtered device class.
     * 
     * @return A {@link String}. <code>null</code> when no filter.
     */
    public String getFilteredDeviceClass() {
        return filteredDeviceClass;
    }

    /**
     * Sets the filtered device class.
     * 
     * @param filteredDeviceClass The filtered device class. <code>null</code> for no filter.
     */
    public void setFilteredDeviceClass(String filteredDeviceClass) {
        String deviceClass = filteredDeviceClass == null ? null : filteredDeviceClass.trim();
        if (deviceClass.isEmpty()) {
            deviceClass = null;
        }
        if (((deviceClass == null) && (this.filteredDeviceClass != null))
                || ((deviceClass != null) && (this.filteredDeviceClass == null)) || ((deviceClass != null)
                        && (this.filteredDeviceClass != null) && !deviceClass.equalsIgnoreCase(filteredDeviceClass))) {
            this.filteredDeviceClass = deviceClass;
            SwingUtilities.invokeLater(() -> {
                TangoNode root = getRoot();
                if ((root != null) && (root.getLastRetryDate() > 0)) {
                    construct();
                }
            });
        }
    }

    /**
     * Removes all the descending nodes from a node, and may remove that node from its parent if asked.
     * <p>
     * <big><b><u>This method won't notify for tree structure changes.</u></b></big>
     * </p>
     * 
     * @param node The node to remove from parent, and for which to remove descending nodes.
     * @param parent The parent {@link MutableTreeNode}.
     * @param removeNodeFromParent Whether to remove the node from its parent.
     */
    protected void removeDescendants(MutableTreeNode node, MutableTreeNode parent, boolean removeNodeFromParent) {
        if (node != null) {
            for (int i = 0; i < node.getChildCount(); i++) {
                TreeNode child = node.getChildAt(i);
                if (child instanceof MutableTreeNode) {
                    removeDescendants((MutableTreeNode) child, node, true);
                }
            }
            if (removeNodeFromParent && (parent != null)) {
                parent.remove(node);
            }
        }
    }

    /**
     * Removes all the descending nodes from a node.
     * <p>
     * <big><b><u>This method won't notify for tree structure changes.</u></b></big>
     * </p>
     * 
     * @param node The node to remove from parent, and for which to remove descending nodes.
     */
    public void removeDescendants(MutableTreeNode node) {
        removeDescendants(node, null, false);
    }

    /**
     * ASynchronous constructs the tree structure, according to given TANGO_HOST.
     */
    public void construct() {
        // Call done in EDT due to step 2
        SwingUtilities.invokeLater(() -> {
            // Step 1 - cancel previous construction SwingWorker
            boolean wasWorking;
            TreeStructureWorker last = this.lastStructureWorker;
            if (last == null) {
                wasWorking = false;
            } else {
                last.setCanceled(true);
                wasWorking = !last.isDone();
            }
            TangoNode root = getRoot();
            if (root != null) {
                if ((!root.isLoading()) || wasWorking) {
                    root.setLoading(true);
                    root.setError(false);
                    root.setLastRetryDate(System.currentTimeMillis());
                    // Step 2 - clear tree structure
                    removeDescendants(root);
                    reload();
                    // Step 3 - construct tree structure
                    TreeStructureWorker worker = new TreeStructureWorker(root);
                    this.lastStructureWorker = worker;
                    worker.execute();
                }
            }
        });
    }

    /**
     * Returns the TANGO_HOST to which this {@link TangoTreeModel} is connected.
     * 
     * @return A {@link String}.
     */
    public String getTangoHost() {
        return tangoHost;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * The {@link SwingWorker} in charge of constructing tree structure according to given TANGO_HOST.
     * 
     * @author GIRARDOT
     */
    protected class TreeStructureWorker extends SwingWorker<List<String>, Void> implements ICancelable {

        protected final WeakReference<TangoNode> nodeReference;
        protected volatile boolean canceled;

        /**
         * Creates a new {@link TreeStructureWorker} that will fill structure under given root {@link TangoNode}.
         * 
         * @param node The root {@link TangoNode}.
         */
        public TreeStructureWorker(TangoNode node) {
            super();
            canceled = false;
            if (node == null) {
                this.nodeReference = null;
            } else {
                this.nodeReference = new WeakReference<>(node);
            }
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
        }

        @Override
        protected List<String> doInBackground() throws Exception {
            List<String> devices = new ArrayList<>();
            TangoNode node = ObjectUtils.recoverObject(nodeReference);
            if ((node != null) && (!node.isError()) && (lastStructureWorker == this)) {
                node.setLoading(true);
                Database db = TangoTreeUtils.getDatabase(node.getFullPath());
                if (db != null) {
                    final String[] deviceNames;
                    String devClass = filteredDeviceClass;
                    if (devClass == null) {
                        deviceNames = db.get_device_list(TangoTreeUtils.TANGO_WILDCARD);
                    } else {
                        deviceNames = db.get_device_exported_for_class(devClass);
                    }
                    if ((!canceled) && (deviceNames != null)) {
                        for (String device : deviceNames) {
                            if (canceled) {
                                break;
                            }
                            if ((device != null) && !device.isEmpty()) {
                                devices.add(device);
                            }
                        }
                    }
                } // end if (db != null)
            } // end if ((node != null) && (!node.isLoading()) && (!node.isError()))
            if ((!canceled) && (!devices.isEmpty())) {
                Collections.sort(devices, Collator.getInstance());
            }
            return devices;
        }

        @Override
        protected void done() {
            TangoNode node = ObjectUtils.recoverObject(nodeReference);
            if ((node != null) && (!canceled)) {
                try {
                    List<String> devices = get();
                    if (!canceled) {
                        if (devices.isEmpty()) {
                            node.setError(true);
                        } else {
                            Map<String, TangoNode> domainMap = new HashMap<>();
                            Map<String, TangoNode> familyMap = new HashMap<>();
                            for (String device : devices) {
                                if (canceled) {
                                    break;
                                }
                                String[] split = device.split(TangoTreeUtils.TANGO_SEPARATOR);
                                if ((!canceled) && (split != null) && split.length > 2) {
                                    final String domain = split[0], domainLow = domain.toLowerCase();
                                    final String family = split[1],
                                            fullFamily = domain + TangoTreeUtils.TANGO_SEPARATOR + family;
                                    final String familyLow = domainLow + TangoTreeUtils.TANGO_SEPARATOR
                                            + family.toLowerCase();
                                    final String member = split[2];
                                    TangoNode domainNode = domainMap.get(domainLow);
                                    if (!canceled) {
                                        if (domainNode == null) {
                                            domainNode = new TangoNode(domain);
                                            domainMap.put(domainLow, domainNode);
                                            node.add(domainNode);
                                        }
                                        TangoNode familyNode = familyMap.get(familyLow);
                                        if (!canceled) {
                                            if (familyNode == null) {
                                                familyNode = new TangoNode(family, fullFamily);
                                                familyMap.put(familyLow, familyNode);
                                                domainNode.add(familyNode);
                                            }
                                            familyNode.add(new TangoNode(member, device));
                                        }
                                    }
                                } // end if ((!canceled) && (split != null) && split.length > 2)
                            } // end for (String device : devices)
                            domainMap.clear();
                            familyMap.clear();
                        } // end if (devices.isEmpty()) ... else
                    } // end if (!canceled)
                } catch (InterruptedException | ExecutionException e) {
                    if (!canceled) {
                        node.setError(true);
                    }
                } finally {
                    if (!canceled) {
                        node.setLoading(false);
                        reload();
                    }
                }
            }
        }
    }

}
