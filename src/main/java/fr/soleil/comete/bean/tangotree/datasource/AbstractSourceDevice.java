package fr.soleil.comete.bean.tangotree.datasource;

public abstract class AbstractSourceDevice implements ISourceDevice{

    private String deviceName = null;
    
    public AbstractSourceDevice(String deviceName) {
        this.deviceName = deviceName;
    }
    
    public AbstractSourceDevice() {
        this(null);
    }

    public String getDeviceName() {
        return deviceName;
    }
    
    
}
