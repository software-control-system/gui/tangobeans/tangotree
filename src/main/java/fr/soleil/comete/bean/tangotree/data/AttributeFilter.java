package fr.soleil.comete.bean.tangotree.data;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A class for something to filter attributes.
 * 
 * @author GIRARDOT
 */
public final class AttributeFilter {

    protected final DataDimension dataDimension;
    protected final DataType dataType;

    public AttributeFilter(DataDimension dataDimension, DataType dataType) {
        super();
        this.dataDimension = dataDimension;
        this.dataType = dataType;
    }

    public DataDimension getDataDimension() {
        return dataDimension;
    }

    public DataType getDataType() {
        return dataType;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if ((obj instanceof AttributeFilter) && (obj.getClass().equals(getClass()))) {
            AttributeFilter filter = (AttributeFilter) obj;
            equals = (filter.getDataDimension() == getDataDimension()) && (filter.getDataType() == getDataType());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xA771B;
        int mult = 0xF117E2;
        code = code * mult + ObjectUtils.getHashCode(dataDimension);
        code = code * mult + ObjectUtils.getHashCode(dataType);
        return code;
    }

}
