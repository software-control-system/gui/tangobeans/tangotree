package fr.soleil.comete.bean.tangotree.node;

import fr.soleil.comete.bean.tangotree.data.DataDimension;
import fr.soleil.comete.bean.tangotree.data.DataType;

public class AttributeNode extends TangoNode {

    private static final long serialVersionUID = 7514808324808562315L;

    private final DataDimension dimension;
    private final DataType dataType;

    public AttributeNode(String shortPath, String fullPath, DataDimension dimension, DataType dataType) {
        super(shortPath, fullPath);
        this.dimension = dimension;
        this.dataType = dataType;
    }

    public DataDimension getDimension() {
        return dimension;
    }

    public DataType getDataType() {
        return dataType;
    }

}
