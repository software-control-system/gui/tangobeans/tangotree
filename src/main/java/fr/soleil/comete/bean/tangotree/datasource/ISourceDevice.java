package fr.soleil.comete.bean.tangotree.datasource;

import java.util.List;

import fr.soleil.comete.bean.tangotree.SourceDeviceException;

public interface ISourceDevice {

    public final static String DEFAULT_SEPARATOR = "/";

    public List<String> getSourceList() throws SourceDeviceException;

    public String getName();

    public String getInformation();

    public String getPathSeparator();

}
