package fr.soleil.comete.bean.tangotree;

import java.util.List;

import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.definition.widget.IComponent;

public interface IDeviceSelector {

    public ISourceDevice getSourceDevice();

    public void setSourceDevice(ISourceDevice sourceDevice);

    public IComponent getSelectorComponent();

    public List<String> getSelectedDevices();

    public void clearSelectedDevices();

    public void addDeviceSelectorListener(IDeviceSelectorListener listener);

    public void removeDeviceSelectorListener(IDeviceSelectorListener listener);

}
