package fr.soleil.comete.bean.tangotree.data;

/**
 * An {@link Enum} that represents the kind of dimensions a tango attribute might have (scalar, spectum or image).
 * 
 * @author GIRARDOT
 *
 */
public enum DataDimension {
    SCALAR, SPECTRUM, IMAGE
}
