package fr.soleil.comete.bean.tangotree.node;

import java.text.Collator;

import javax.swing.tree.DefaultMutableTreeNode;

public class TangoNode extends DefaultMutableTreeNode implements Comparable<TangoNode> {

    private static final long serialVersionUID = 3141069451417164298L;

    protected volatile boolean loading, error;
    protected volatile long lastErrorDate;
    protected volatile long lastRetryDate;
    protected final String shortPath, fullPath;

    public TangoNode(String path) {
        this(path, path);
    }

    public TangoNode(String shortPath, String fullPath) {
        super(fullPath == null ? shortPath : fullPath);
        this.shortPath = shortPath == null ? fullPath : shortPath;
        this.fullPath = (String) getUserObject();
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public boolean isLoading() {
        return loading;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
        if (error) {
            lastErrorDate = System.currentTimeMillis();
        }
    }

    public long getLastErrorDate() {
        return lastErrorDate;
    }

    public long getLastRetryDate() {
        return lastRetryDate;
    }

    public void setLastRetryDate(long lastRetryDate) {
        this.lastRetryDate = lastRetryDate;
    }

    public String getShortPath() {
        return shortPath;
    }

    public String getFullPath() {
        return fullPath;
    }

    @Override
    public int compareTo(TangoNode o) {
        int comp;
        if (o == null) {
            comp = 1;
        } else if (fullPath == null) {
            comp = o.fullPath == null ? 0 : -1;
        } else if (o.fullPath == null) {
            comp = 1;
        } else {
            comp = Collator.getInstance().compare(fullPath, o.fullPath);
        }
        return comp;
    }
}
