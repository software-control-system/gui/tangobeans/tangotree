package fr.soleil.comete.bean.tangotree;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.ref.WeakReference;
import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.comete.bean.tangotree.data.AttributeFilter;
import fr.soleil.comete.bean.tangotree.data.DataDimension;
import fr.soleil.comete.bean.tangotree.data.DataLevel;
import fr.soleil.comete.bean.tangotree.data.DataType;
import fr.soleil.comete.bean.tangotree.model.TangoTreeModel;
import fr.soleil.comete.bean.tangotree.node.AttributeNode;
import fr.soleil.comete.bean.tangotree.node.TangoNode;
import fr.soleil.comete.bean.tangotree.renderer.TangoTreeRenderer;
import fr.soleil.comete.bean.tangotree.util.TangoTreeUtils;
import fr.soleil.comete.swing.Label;
import fr.soleil.data.controller.BasicTextTargetController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.tree.ExpandableTree;

/**
 * An {@link ExpandableTree} that allows browsing tango database and will send selected data (device full name or
 * attribute full name) in an {@link AbstractDataSource}.
 * <p>
 * Data is considered as selected if user double clicked on associated node.
 * </p>
 * 
 * @author GIRARDOT
 */
public class TangoTree extends ExpandableTree implements MouseListener, KeyListener, TreeModelListener {

    private static final long serialVersionUID = 7044104065116800780L;

    /**
     * The default period to wait before trying again to obtain the children of a node in case of failure or when
     * <code>[F5]</code> key is pressed.
     */
    public static final long DEFAULT_RETRY_PERIOD = 30000; // 30s

    protected final AbstractDataSource<String> selectedDataSource;
    private DataLevel selectableDataLevel;
    private long retryPeriod;
    private WeakReference<TangoTreeModel> tangoTreeModelRef;
    private AttributeFilter[] attributeFilters;
    protected volatile String dataToSelect;
    protected boolean autoRetry;

    public TangoTree() {
        this(new TangoTreeModel());
    }

    public TangoTree(TangoTreeModel model) {
        super(model == null ? new TangoTreeModel() : model);
        this.autoRetry = true;
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        setShowsRootHandles(true);
        setCellRenderer(new TangoTreeRenderer());
        BasicDataSource<String> source = new BasicDataSource<>(null);
        source.setIgnoreDuplicationTest(true);
        selectedDataSource = source;
        selectableDataLevel = DataLevel.DEVICE;
        this.retryPeriod = DEFAULT_RETRY_PERIOD;
        dataToSelect = null;
        addMouseListener(this);
        addKeyListener(this);
    }

    @Override
    public void setModel(TreeModel newModel) {
        if (newModel instanceof TangoTreeModel) {
            TreeModel oldModel = getModel();
            if (oldModel != null) {
                oldModel.removeTreeModelListener(this);
            }
            TangoTreeModel model = (TangoTreeModel) newModel;
            tangoTreeModelRef = new WeakReference<>(model);
            super.setModel(model);
            model.addTreeModelListener(this);
            model.construct();
//            repaint();
        }
    }

    /**
     * Returns the {@link TangoTreeModel} used by this {@link TangoTree}.
     * 
     * @return A {@link TangoTreeModel}. Might be <code>null</code>.
     */
    protected final TangoTreeModel getTangoTreeModel() {
        return ObjectUtils.recoverObject(tangoTreeModelRef);
    }

    @Override
    public void setCellRenderer(TreeCellRenderer renderer) {
        if (renderer instanceof TangoTreeRenderer) {
            super.setCellRenderer(renderer);
        }
    }

    protected void updateSelection(String... split) {
        if (split != null) {
            TangoTreeModel model = ObjectUtils.recoverObject(tangoTreeModelRef);
            if (model != null) {
                TangoNode root = model.getRoot();
                if (root != null) {
                    int index = 0;
                    for (int i = 0; i < root.getChildCount(); i++) {
                        if (updateSelection((TangoNode) root.getChildAt(i), index, split)) {
                            break;
                        }
                    }
                }
            }
        }
    }

    protected boolean updateSelection(TangoNode node, int index, String... split) {
        boolean result = false;
        if (index < split.length) {
            String shortName = split[index];
            if ((shortName != null) && (node.getShortPath() != null)
                    && shortName.equalsIgnoreCase(node.getShortPath())) {
                if (index == split.length - 1) {
                    dataToSelect = null;
                    result = true;
                    // Expand path
                    TreePath path = new TreePath(node.getPath());
                    expandPath(path);
                    // Select node
                    getSelectionModel().setSelectionPath(path);
                } else {
                    index++;
                    TreePath path = new TreePath(node.getPath());
                    if (node.isLeaf()) {
                        // Select node (to show something is happening)
                        getSelectionModel().setSelectionPath(path);
                        // Load device attributes
                        TangoTreeModel model = ObjectUtils.recoverObject(tangoTreeModelRef);
                        if (model != null) {
                            new AttributesLoader(model.getTangoHost(), node, path).execute();
                        }
                    } else {
                        // Expand path
                        expandPath(path);
                        for (int i = 0; i < node.getChildCount() && (!result); i++) {
                            result = updateSelection((TangoNode) node.getChildAt(i), index, split);
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Selects the node that matches given {@link String}.
     * <p>
     * This will work only if given {@link String} matches a valid data that can be selected, according to
     * {@link #getSelectableDataLevel()}.
     * </p>
     * 
     * @param dataToSelect The full name of the device or attribute to select.
     * @see #getSelectableDataLevel()
     * @see #setSelectableDataLevel(DataLevel)
     */
    public void selectData(String dataToSelect) {
        if (dataToSelect != null) {
            String[] split = dataToSelect.split(TangoTreeUtils.TANGO_SEPARATOR);
            if (split.length == 3) {
                if (selectableDataLevel == DataLevel.DEVICE) {
                    this.dataToSelect = dataToSelect;
                } else {
                    split = null;
                }
            } else if (split.length == 4) {
                if (selectableDataLevel == DataLevel.ATTRIBUTE) {
                    this.dataToSelect = dataToSelect;
                } else {
                    split = null;
                }
            } else {
                split = null;
            }
            final String[] toSelect = split;
            if (toSelect != null) {
                SwingUtilities.invokeLater(() -> {
                    updateSelection(toSelect);
                });
            }
        }
    }

    /**
     * Returns the period, in milliseconds, to wait before retrying to obtain the children of a node (might concern
     * either the root node or a device node).
     * 
     * @return A strictly positive <code>long</code>.
     */
    public long getRetryPeriod() {
        return retryPeriod;
    }

    /**
     * Sets the period, in milliseconds, to wait before retrying to obtain the children of a node (might concern either
     * the root node or a device node).
     * 
     * @param retryPeriod The period to wait for.
     *            <p>
     *            If <code>retryPeriod</code> is &le; <code>0</code>, then {@link #DEFAULT_RETRY_PERIOD}
     *            will be used.
     *            </p>
     */
    public void setRetryPeriod(long retryPeriod) {
        this.retryPeriod = retryPeriod > 0 ? retryPeriod : DEFAULT_RETRY_PERIOD;
    }

    /**
     * Return whether automatic retry (retry to obtain children node when clicking on it) is enabled.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isAutoRetry() {
        return autoRetry;
    }

    /**
     * Sets whether automatic retry (retry to obtain children node when clicking on it) should be enabled.
     * 
     * @param autoRetry Whether automatic retry should be enabled.
     */
    public void setAutoRetry(boolean autoRetry) {
        this.autoRetry = autoRetry;
    }

    /**
     * Returns the source that will notify for data selection.
     * 
     * @return An {@link AbstractDataSource}.
     */
    public AbstractDataSource<String> getSelectedDataSource() {
        return selectedDataSource;
    }

    /**
     * Returns the type of data that can be selected (sent to {@link #getSelectedDataSource()}).
     * 
     * @return A {@link DataLevel}.
     *         <ul>
     *         <li>If {@link DataLevel#DEVICE}, then {@link #getSelectedDataSource()} will be updated every time a
     *         double click is done on a device. This implies that the tree won't browse device attributes.</li>
     *         <li>If {@link DataLevel#ATTRIBUTE}, then {@link #getSelectedDataSource()} will be updated every time a
     *         double click is done on an attribute. This implies that the tree will browse device attributes.</li>
     *         </ul>
     * @see #setSelectableDataLevel(DataLevel)
     */
    public DataLevel getSelectableDataLevel() {
        return selectableDataLevel;
    }

    /**
     * Sets the type of data, represented as a {@link DataLevel}, that can be selected (sent to
     * {@link #getSelectedDataSource()}).
     * 
     * @param selectableDataLevel The {@link DataLevel} to set.
     *            <ul>
     *            <li>If {@link DataLevel#DEVICE}, then {@link #getSelectedDataSource()} will be updated every time a
     *            double click is done on a device. This implies that the tree won't browse device attributes.</li>
     *            <li>If {@link DataLevel#ATTRIBUTE}, then {@link #getSelectedDataSource()} will be updated every time a
     *            double click is done on an attribute. This implies that the tree will browse device attributes.</li>
     *            </ul>
     */
    public void setSelectableDataLevel(DataLevel selectableDataLevel) {
        this.selectableDataLevel = selectableDataLevel == null ? DataLevel.DEVICE : selectableDataLevel;
    }

    /**
     * Returns how attributes are filtered through.
     * 
     * @return An {@link AttributeFilter} array.
     */
    public AttributeFilter[] getAttributeFilters() {
        AttributeFilter[] filters = this.attributeFilters;
        return filters == null ? null : filters.clone();
    }

    /**
     * Sets how attributes should be filtered through {@link AttributeFilter}s.
     * 
     * @param filters The {@link AttributeFilter}s that define how to filter attributes.
     */
    public void setAttributeFilters(AttributeFilter... filters) {
        this.attributeFilters = filters == null ? null : filters.clone();
    }

    /**
     * Returns the filtered device class.
     * 
     * @return A {@link String}. <code>null</code> when no filter.
     */
    public String getFilteredDeviceClass() {
        TangoTreeModel model = getTangoTreeModel();
        return model == null ? null : model.getFilteredDeviceClass();
    }

    /**
     * Sets the filtered device class.
     * 
     * @param filteredDeviceClass The filtered device class. <code>null</code> for no filter.
     */
    public void setFilteredDeviceClass(String filteredDeviceClass) {
        TangoTreeModel model = getTangoTreeModel();
        if (model != null) {
            model.setFilteredDeviceClass(filteredDeviceClass);
        }
    }

    /**
     * Returns whether it is allowed to retry discovering the children of a {@link TangoNode}.
     * 
     * @param node The {@link TangoNode}. Can not be <code>null</code>.
     * @return A <code>boolean</code>.
     */
    protected boolean mayRetry(TangoNode node) {
        long time = System.currentTimeMillis();
        return (time - node.getLastErrorDate() >= retryPeriod) || (time - node.getLastRetryDate() >= retryPeriod);
    }

    /**
     * Common part in {@link TreeModelEvent} treatment.
     * 
     * @param e The {@link TreeModelEvent}.
     */
    protected void manageTreeModelEvent(TreeModelEvent e) {
        if (e != null) {
            TreePath path = e.getTreePath();
            if (path != null) {
                expandPath(path);
                selectData(dataToSelect);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if ((e != null) && (e.getSource() == this) && (e.getClickCount() == 2)) {
            TreePath path = getPathForLocation(e.getX(), e.getY());
            if (path != null) {
                Object comp = path.getLastPathComponent();
                if (comp instanceof TangoNode) {
                    TangoNode node = (TangoNode) comp;
                    int depth = TangoTreeUtils.getDepth(node);
                    if ((selectableDataLevel == DataLevel.DEVICE && depth == 3)
                            || (selectableDataLevel == DataLevel.ATTRIBUTE && depth == 4)) {
                        try {
                            selectedDataSource.setData(node.getFullPath());
                        } catch (Exception e1) {
                            // Will not happen with BasicDataSource;
                        }
                    }
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if ((e != null) && (e.getSource() == this) && (!e.isAltDown()) && (!e.isControlDown()) && (!e.isShiftDown())
                && (!e.isAltGraphDown()) && (!e.isConsumed())) {
            TreePath path = getPathForLocation(e.getX(), e.getY());
            if (path != null) {
                Object comp = path.getLastPathComponent();
                TangoTreeModel model = getTangoTreeModel();
                if ((comp instanceof TangoNode) && (model != null)) {
                    TangoNode node = (TangoNode) comp;
                    if ((!node.isLoading()) && mayRetry(node) && node.isLeaf() && ((!node.isError()) || isAutoRetry())
                            && (TangoTreeUtils.getDepth(node) == 3) && (selectableDataLevel == DataLevel.ATTRIBUTE)) {
                        // Load device attributes
                        new AttributesLoader(model.getTangoHost(), node, path).execute();
                        repaint();
                    } else if (node.isRoot() && node.isLeaf() && node.isError() && isAutoRetry() && (!node.isLoading())
                            && mayRetry(node)) {
                        // Reload
                        model.construct();
                    }
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // nothing to do
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if ((e != null) && (e.getSource() == this) && (e.getKeyCode() == KeyEvent.VK_F5)) {
            TreePath path = getSelectionPath();
            if (path != null) {
                Object comp = path.getLastPathComponent();
                TangoTreeModel model = getTangoTreeModel();
                if ((comp instanceof TangoNode) && (model != null)) {
                    TangoNode node = (TangoNode) comp;
                    if (!node.isLoading() && (TangoTreeUtils.getDepth(node) == 3) && mayRetry(node)) {
                        SwingUtilities.invokeLater(() -> {
                            node.setLoading(true);
                            node.setLastRetryDate(System.currentTimeMillis());
                            model.removeDescendants(node);
                            model.reload(node);
                            setSelectionPath(new TreePath(node.getPath()));
                            // Load device attributes
                            new AttributesLoader(model.getTangoHost(), node, path).execute();
                        });
                    } else if (node.isRoot() && (!node.isLoading()) && mayRetry(node)) {
                        // Reload
                        model.construct();
                    }
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // nothing to do
    }

    @Override
    public void treeNodesChanged(TreeModelEvent e) {
        manageTreeModelEvent(e);
    }

    @Override
    public void treeNodesInserted(TreeModelEvent e) {
        manageTreeModelEvent(e);
    }

    @Override
    public void treeNodesRemoved(TreeModelEvent e) {
        manageTreeModelEvent(e);
    }

    @Override
    public void treeStructureChanged(TreeModelEvent e) {
        TreePath path = getSelectionPath();
        manageTreeModelEvent(e);
        if ((path != null) && (getSelectionPath() == null)) {
            setSelectionPath(path);
        }
    }

    public static void main(String[] args) {
        Label dataLabel = new Label();
        dataLabel.setFont(new Font(Font.DIALOG, Font.PLAIN, 12));
        JLabel titleLabel = new JLabel("Selected data:");
        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel dataPanel = new JPanel(new BorderLayout(5, 5));
        dataPanel.add(titleLabel, BorderLayout.WEST);
        dataPanel.add(dataLabel, BorderLayout.CENTER);
        mainPanel.add(dataPanel, BorderLayout.SOUTH);
        JFrame testFrame = new JFrame(TangoTree.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setSize(500, 400);
        testFrame.setLocationRelativeTo(null);
        TangoTree tree = new TangoTree();
        tree.setSelectableDataLevel(DataLevel.ATTRIBUTE);
//        tree.setFilteredDeviceClass("TangoTest");
//        tree.setFilteredAttibuteDimension(DataDimension.SCALAR);
//        tree.setFilteredAttibuteType(DataType.STRING);
        mainPanel.add(new JScrollPane(tree), BorderLayout.CENTER);
        BasicTextTargetController controller = new BasicTextTargetController();
        controller.addLink(tree.getSelectedDataSource(), dataLabel);
        testFrame.setContentPane(mainPanel);
        testFrame.setVisible(true);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link SwingWorker} that will load device attributes and fill matching tree structure.
     * 
     * @author GIRARDOT
     */
    protected class AttributesLoader extends SwingWorker<List<AttributeNode>, Void> {

        protected final String tangoHost;
        protected final WeakReference<TangoNode> nodeRef;
        protected final WeakReference<TreePath> pathRef;
        protected boolean attributesFound;

        /**
         * Creates a new {@link AttributesLoader}, that will search for the device that matches given {@link TangoNode}
         * in given TANGO_HOST, and fill the corresponding tree structure.
         * 
         * @param tangoHost The TANGO_HOST.
         * @param node The {@link TangoNode}.
         * @param path The {@link TreePath} of <code>node</code>. Necessary to expand it ones structure is loaded.
         */
        public AttributesLoader(String tangoHost, TangoNode node, TreePath path) {
            super();
            this.tangoHost = tangoHost;
            if (node == null) {
                this.nodeRef = null;
            } else {
                node.setError(false);
                node.setLoading(true);
                node.setLastRetryDate(System.currentTimeMillis());
                this.nodeRef = new WeakReference<>(node);
            }
            this.pathRef = path == null ? null : new WeakReference<>(path);
            this.attributesFound = false;
        }

        @Override
        protected List<AttributeNode> doInBackground() throws Exception {
            List<AttributeNode> nodes = new ArrayList<>();
            TangoNode node = ObjectUtils.recoverObject(nodeRef);
            if ((tangoHost != null) && (node != null)) {
                DeviceProxy proxy = TangoTreeUtils.getDeviceProxy(tangoHost, node.getFullPath());
                Map<String, AttributeNode> nodeMap = new TreeMap<>(Collator.getInstance());
                AttributeInfo[] infoList = proxy.get_attribute_info();
                if (infoList != null) {
                    for (AttributeInfo info : infoList) {
                        if (info != null) {
                            attributesFound = true;
                            DataDimension dimension;
                            if (info.data_format == null) {
                                dimension = null;
                            } else {
                                switch (info.data_format.value()) {
                                    case AttrDataFormat._SCALAR:
                                        dimension = DataDimension.SCALAR;
                                        break;
                                    case AttrDataFormat._SPECTRUM:
                                        dimension = DataDimension.SPECTRUM;
                                        break;
                                    case AttrDataFormat._IMAGE:
                                        dimension = DataDimension.IMAGE;
                                        break;
                                    default:
                                        dimension = null;
                                        break;
                                }
                            }
                            DataType type;
                            switch (info.data_type) {
                                case TangoConst.Tango_DEV_BOOLEAN:
                                    type = DataType.BOOLEAN;
                                    break;
                                case TangoConst.Tango_DEV_STRING:
                                    type = DataType.STRING;
                                    break;
                                case TangoConst.Tango_DEV_STATE:
                                    type = DataType.STATE;
                                    break;
                                case TangoConst.Tango_DEV_CHAR:
                                case TangoConst.Tango_DEV_UCHAR:
                                case TangoConst.Tango_DEV_SHORT:
                                case TangoConst.Tango_DEV_USHORT:
                                case TangoConst.Tango_DEV_INT:
                                case TangoConst.Tango_DEV_LONG:
                                case TangoConst.Tango_DEV_ULONG:
                                case TangoConst.Tango_DEV_LONG64:
                                case TangoConst.Tango_DEV_ULONG64:
                                case TangoConst.Tango_DEV_FLOAT:
                                case TangoConst.Tango_DEV_DOUBLE:
                                    type = DataType.NUMBER;
                                    break;
                                default:
                                    type = null;
                                    break;
                            } // end switch (info.data_type)
                            AttributeFilter[] filters = attributeFilters;
                            boolean mayAdd;
                            if ((filters == null) || (filters.length == 0)) {
                                mayAdd = true;
                            } else {
                                mayAdd = false;
                                for (AttributeFilter filter : filters) {
                                    if (filter == null) {
                                        mayAdd = true;
                                        break;
                                    } else {
                                        DataType filteredAttibuteType = filter.getDataType();
                                        DataDimension filteredAttibuteDimension = filter.getDataDimension();
                                        if (((filteredAttibuteType == null) || (type == filteredAttibuteType))
                                                && ((filteredAttibuteDimension == null)
                                                        || (dimension == filteredAttibuteDimension))) {
                                            mayAdd = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (mayAdd) {
                                nodeMap.put(info.name,
                                        new AttributeNode(info.name,
                                                node.getFullPath() + TangoTreeUtils.TANGO_SEPARATOR + info.name,
                                                dimension, type));
                            }
                        }
                    } // end for (AttributeInfo info : infoList)
                    for (AttributeNode child : nodeMap.values()) {
                        nodes.add(child);
                    }
                    nodeMap.clear();
                } // end if (infoList != null)
            }
            return nodes;
        }

        @Override
        protected void done() {
            TangoNode node = ObjectUtils.recoverObject(nodeRef);
            if (node != null) {
                try {
                    List<AttributeNode> nodes = get();
                    if (nodes.isEmpty()) {
                        if (!attributesFound) {
                            node.setError(true);
                        }
                    } else {
                        for (AttributeNode child : nodes) {
                            node.add(child);
                        }
                        nodes.clear();
                        TangoTreeModel model = getTangoTreeModel();
                        if (model != null) {
                            model.reload(node);
                            TreePath path = ObjectUtils.recoverObject(pathRef);
                            if (path != null) {
                                SwingUtilities.invokeLater(() -> {
                                    expandPath(path);
                                });
                            }
                        }
                    }
                } catch (InterruptedException | ExecutionException e) {
                    node.setError(true);
                } finally {
                    node.setLoading(false);
                    repaint();
                }
            }
        }
    }

}
