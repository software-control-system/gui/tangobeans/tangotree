package fr.soleil.comete.bean.tangotree;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.bean.tangotree.datasource.DatabaseTangoSourceDevice;
import fr.soleil.comete.bean.tangotree.datasource.GenericSourceDevice;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.bean.tangotree.datasource.SnapshotSourceDevice;
import fr.soleil.comete.bean.tangotree.swing.SwingDeviceSelector;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.CometeWidgetFactory;
import fr.soleil.comete.swing.Panel;

public class DemoSnapshot {

    private static SwingDeviceSelector archivingSelector;
    private static SwingDeviceSelector genericSelector;
    private static SwingDeviceSelector tangoSelector;

    /**
     * Demo
     * 
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) {
        try {
            JFrame frame = new JFrame();
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            URL file = frame.getClass().getResource("/com/famfamfam/silk/book.png");
            CometeImage bookImage = new CometeImage(file.toString());

            URL file1 = frame.getClass().getResource("/com/famfamfam/silk/cancel.png");
            CometeImage cancelImage = new CometeImage(file1.toString());

            URL file2 = frame.getClass().getResource("/com/famfamfam/silk/computer.png");
            final CometeImage computerImage = new CometeImage(file2.toString());

            // JPanel borderPanel = new JPanel(new BorderLayout());

            JPanel gridPanel = new JPanel();
            gridPanel.setLayout(new GridLayout(1, 1));
            CometeWidgetFactory.setCurrentPackage(CometeWidgetFactory.CometePackage.SWING);

            Panel selectOk1pan = new Panel();
            selectOk1pan.setLayout(new BorderLayout());
            ISourceDevice archivingSource = new SnapshotSourceDevice("archiving/snap/snapextractor.1");
            archivingSelector = new SwingDeviceSelector(archivingSource, true, computerImage, bookImage, cancelImage,
                    false);
            archivingSelector.setLeafImage(cancelImage);
            archivingSelector.setNodeImage(bookImage);
            archivingSelector.setRootImage(computerImage);

            selectOk1pan.add(archivingSelector.getSelectorComponent(), BorderLayout.CENTER);
            JButton okButton1 = new JButton("ok");
            okButton1.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(final ActionEvent e) {
                    archivingSelector.setNodeImage(computerImage);
                    getSelectedDevices(archivingSelector);

                }
            });
            selectOk1pan.add(okButton1, BorderLayout.SOUTH);
            gridPanel.add(selectOk1pan);

            Panel selectOk2pan = new Panel();
            selectOk2pan.setLayout(new BorderLayout());
            ISourceDevice genericSource = new GenericSourceDevice("archiving/hdb-oracle/hdbextractor.1/GetAttNameAll");
            genericSelector = new SwingDeviceSelector(genericSource, true, bookImage, cancelImage, null, true);
            // genericSelector.setLeafImage(cancelImage);
            // genericSelector.setNodeImage(bookImage);
            selectOk2pan.add(genericSelector.getSelectorComponent(), BorderLayout.CENTER);
            JButton okButton2 = new JButton("ok");
            okButton2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    getSelectedDevices(genericSelector);

                }
            });
            selectOk2pan.add(okButton2, BorderLayout.SOUTH);
            gridPanel.add(selectOk2pan);

            Panel selectOk3pan = new Panel();
            selectOk3pan.setLayout(new BorderLayout());
            ISourceDevice tangoSource = new DatabaseTangoSourceDevice(true, true);
            tangoSelector = new SwingDeviceSelector(tangoSource, true, true);
            selectOk3pan.add(tangoSelector.getSelectorComponent(), BorderLayout.CENTER);
            JButton okButton3 = new JButton("ok");
            okButton3.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(final ActionEvent e) {
                    getSelectedDevices(tangoSelector);

                }
            });
            selectOk3pan.add(okButton3, BorderLayout.SOUTH);
            gridPanel.add(selectOk3pan);

            frame.add(gridPanel);
            frame.setSize(1600, 800);
            frame.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected static void getSelectedDevices(final SwingDeviceSelector panselect) {
        if (panselect != null) {
            List<String> selectedDevices = panselect.getSelectedDevices();
            if ((selectedDevices != null) && !selectedDevices.isEmpty()) {
                System.out.println("Selected =" + Arrays.toString(selectedDevices.toArray()));
            } else {
                System.out.println("Selection is empty");
            }
        }
    }
}
