package fr.soleil.comete.bean.tangotree.renderer;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultTreeCellRenderer;

import fr.soleil.comete.bean.tangotree.TangoTree;
import fr.soleil.comete.bean.tangotree.data.DataDimension;
import fr.soleil.comete.bean.tangotree.data.DataType;
import fr.soleil.comete.bean.tangotree.node.AttributeNode;
import fr.soleil.comete.bean.tangotree.node.TangoNode;
import fr.soleil.comete.bean.tangotree.util.TangoTreeUtils;
import fr.soleil.lib.project.swing.icons.DecorableIcon;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * The {@link DefaultTreeCellRenderer} used by {@link TangoTree}
 * 
 * @author GIRARDOT
 */
public class TangoTreeRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 1364011334456942936L;

    protected static final Icons ICONS = new Icons("fr.soleil.comete.bean.tangotree.icons");

    protected static final ImageIcon LOADING_ICON = TangoTreeUtils.ICONS.getIcon("tangotree.decoration.loading");
    protected static final ImageIcon ERROR_ICON = TangoTreeUtils.ICONS.getIcon("tangotree.decoration.error");
    protected static final ImageIcon ROOT_ICON = TangoTreeUtils.ICONS.getIcon("tangotree.root");
    protected static final ImageIcon DEVICE_ICON = TangoTreeUtils.ICONS.getIcon("tangotree.device");
    protected static final ImageIcon ATTRIBUTE_DEFAULT_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.default");
    protected static final ImageIcon ATTRIBUTE_NUMBER_SCALAR_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.number.scalar");
    protected static final ImageIcon ATTRIBUTE_NUMBER_SPECTRUM_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.number.spectrum");
    protected static final ImageIcon ATTRIBUTE_NUMBER_IMAGE_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.number.image");
    protected static final ImageIcon ATTRIBUTE_BOOLEAN_SCALAR_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.boolean.scalar");
    protected static final ImageIcon ATTRIBUTE_BOOLEAN_SPECTRUM_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.boolean.spectrum");
    protected static final ImageIcon ATTRIBUTE_BOOLEAN_IMAGE_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.boolean.image");
    protected static final ImageIcon ATTRIBUTE_STRING_SCALAR_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.string.scalar");
    protected static final ImageIcon ATTRIBUTE_STRING_SPECTRUM_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.string.spectrum");
    protected static final ImageIcon ATTRIBUTE_STRING_IMAGE_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.string.image");
    protected static final ImageIcon ATTRIBUTE_STATE_SCALAR_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.state.scalar");
    protected static final ImageIcon ATTRIBUTE_STATE_SPECTRUM_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.state.spectrum");
    protected static final ImageIcon ATTRIBUTE_STATE_IMAGE_ICON = TangoTreeUtils.ICONS
            .getIcon("tangotree.attribute.state.image");

    protected static final String LOADING_KEY = "fr.soleil.comete.bean.tangotree.loading";
    protected static final String LOADING_ERROR_KEY = "fr.soleil.comete.bean.tangotree.loading.error";

    protected static final Border ATTRIBUTE_BORDER = new EmptyBorder(2, 2, 2, 2);

    public TangoTreeRenderer() {
        super();
    }

    protected void configureDecorableIcon(DecorableIcon decorableIcon) {
        decorableIcon.setDecorationHorizontalAlignment(SwingConstants.RIGHT);
        decorableIcon.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        decorableIcon.setDecorated(true);
    }

    /**
     * Returns a {@link DecorableIcon}, with a loading aspect, based on given {@link ImageIcon}.
     * 
     * @param icon The {@link ImageIcon}.
     * @return A {@link DecorableIcon}.
     */
    protected DecorableIcon buildLoadingIcon(ImageIcon icon) {
        DecorableIcon decorableIcon;
        if (icon == null) {
            decorableIcon = new DecorableIcon();
        } else {
            decorableIcon = new DecorableIcon(icon.getImage());
        }
        decorableIcon.setDecoration(LOADING_ICON);
        configureDecorableIcon(decorableIcon);
        return decorableIcon;
    }

    /**
     * Returns a {@link DecorableIcon}, with an error aspect, based on given {@link ImageIcon}.
     * 
     * @param icon The {@link ImageIcon}.
     * @return A {@link DecorableIcon}.
     */
    protected DecorableIcon buildErrorIcon(ImageIcon icon) {
        DecorableIcon decorableIcon;
        if (icon == null) {
            decorableIcon = new DecorableIcon();
        } else {
            decorableIcon = new DecorableIcon(icon.getImage());
        }
        decorableIcon.setDecoration(ERROR_ICON);
        configureDecorableIcon(decorableIcon);
        return decorableIcon;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
            int row, boolean hasFocus) {
        JLabel comp = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        if (value instanceof TangoNode) {
            TangoNode node = (TangoNode) value;
            int depth = TangoTreeUtils.getDepth(node);
            switch (depth) {
                case 0:
                    // root node: use root icon and no border
                    comp.setIcon(ROOT_ICON);
                    comp.setBorder(null);
                    break;
                case 3:
                    // device node: use device icon and no border
                    comp.setIcon(DEVICE_ICON);
                    comp.setBorder(null);
                    break;
                case 4:
                    // attribute node: find best icon and use spaced border
                    if (node instanceof AttributeNode) {
                        AttributeNode attributeNode = (AttributeNode) node;
                        DataDimension dim = attributeNode.getDimension();
                        DataType type = attributeNode.getDataType();
                        if ((type == null) || (dim == null)) {
                            comp.setIcon(ATTRIBUTE_DEFAULT_ICON);
                        } else {
                            switch (type) {
                                case BOOLEAN:
                                    switch (dim) {
                                        case SCALAR:
                                            comp.setIcon(ATTRIBUTE_BOOLEAN_SCALAR_ICON);
                                            break;
                                        case SPECTRUM:
                                            comp.setIcon(ATTRIBUTE_BOOLEAN_SPECTRUM_ICON);
                                            break;
                                        case IMAGE:
                                            comp.setIcon(ATTRIBUTE_BOOLEAN_IMAGE_ICON);
                                            break;
                                        default:
                                            comp.setIcon(ATTRIBUTE_DEFAULT_ICON);
                                            break;
                                    }
                                    break;
                                case NUMBER:
                                    switch (dim) {
                                        case SCALAR:
                                            comp.setIcon(ATTRIBUTE_NUMBER_SCALAR_ICON);
                                            break;
                                        case SPECTRUM:
                                            comp.setIcon(ATTRIBUTE_NUMBER_SPECTRUM_ICON);
                                            break;
                                        case IMAGE:
                                            comp.setIcon(ATTRIBUTE_NUMBER_IMAGE_ICON);
                                            break;
                                        default:
                                            comp.setIcon(ATTRIBUTE_DEFAULT_ICON);
                                            break;
                                    }
                                    break;
                                case STRING:
                                    switch (dim) {
                                        case SCALAR:
                                            comp.setIcon(ATTRIBUTE_STRING_SCALAR_ICON);
                                            break;
                                        case SPECTRUM:
                                            comp.setIcon(ATTRIBUTE_STRING_SPECTRUM_ICON);
                                            break;
                                        case IMAGE:
                                            comp.setIcon(ATTRIBUTE_STRING_IMAGE_ICON);
                                            break;
                                        default:
                                            comp.setIcon(ATTRIBUTE_DEFAULT_ICON);
                                            break;
                                    }
                                    break;
                                case STATE:
                                    switch (dim) {
                                        case SCALAR:
                                            comp.setIcon(ATTRIBUTE_STATE_SCALAR_ICON);
                                            break;
                                        case SPECTRUM:
                                            comp.setIcon(ATTRIBUTE_STATE_SPECTRUM_ICON);
                                            break;
                                        case IMAGE:
                                            comp.setIcon(ATTRIBUTE_STATE_IMAGE_ICON);
                                            break;
                                        default:
                                            comp.setIcon(ATTRIBUTE_DEFAULT_ICON);
                                            break;
                                    }
                                    break;
                                default:
                                    comp.setIcon(ATTRIBUTE_DEFAULT_ICON);
                                    break;
                            }
                        }
                    } else {
                        comp.setIcon(ATTRIBUTE_DEFAULT_ICON);
                    }
                    comp.setBorder(ATTRIBUTE_BORDER);
                    break;
                default:
                    // other cases: use no border.
                    comp.setBorder(null);
                    break;
            }
            // use short path as text, but full path as tooltip
            comp.setText(node.getShortPath());
            if (node.isLoading()) {
                // adapt tooltip if node is loading
                comp.setToolTipText(
                        String.format(TangoTreeUtils.MESSAGE_MANAGER.getMessage(LOADING_KEY), node.getFullPath()));
            } else if (node.isError()) {
                // adapt tooltip in case of error
                comp.setToolTipText(String.format(TangoTreeUtils.MESSAGE_MANAGER.getMessage(LOADING_ERROR_KEY),
                        node.getFullPath()));
            } else {
                comp.setToolTipText(node.getFullPath());
            }
            if (tree != null) {
                tree.setToolTipText(comp.getToolTipText());
            }
            Icon icon = comp.getIcon();
            if (icon instanceof ImageIcon) {
                ImageIcon imageIcon = (ImageIcon) icon;
                if (node.isError()) {
                    // adapt icon in case of error
                    comp.setIcon(buildErrorIcon(imageIcon));
                } else if (node.isLoading()) {
                    // adapt icon if node is loading
                    comp.setIcon(buildLoadingIcon(imageIcon));
                }
            }
        }
        return comp;
    }

}
