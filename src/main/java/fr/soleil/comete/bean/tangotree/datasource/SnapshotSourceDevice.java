package fr.soleil.comete.bean.tangotree.datasource;

import java.util.ArrayList;
import java.util.List;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.soleil.comete.bean.tangotree.SourceDeviceException;

public class SnapshotSourceDevice extends AbstractSourceDevice {

    private static String extractorName = "SnapExtractor";
    private static String command = "GetSnapshotAttributeList";

    public SnapshotSourceDevice(String deviceName) {
        super(deviceName);
    }
    
    public SnapshotSourceDevice() {
        super();
    }
    
    @Override
    public List<String> getSourceList() throws SourceDeviceException {
        List<String> sourceList = new ArrayList<String>();
        String deviceName = getDeviceName();
        if(deviceName == null){
            String[] deviceNameList = null;
            try {
                Database database = new Database();
                
                deviceNameList = database.get_device_exported_for_class(extractorName);
            } catch (DevFailed e) {
                throw new SourceDeviceException(DevFailedUtils.toString(e));
            }

            if ((deviceNameList != null) && (deviceNameList.length > 0)) {
                deviceName = deviceNameList[0];
            }
        }
        
        if ((deviceName != null) && (!deviceName.isEmpty())) {
            ISourceDevice source = new GenericSourceDevice(deviceName + "/" + command);
            sourceList = source.getSourceList();

        }
        return sourceList;
    }

    @Override
    public String getName() {
        return getDeviceName() + "/" + command;
    }

    @Override
    public String getInformation() {
        return "Snapshot Database";
    }

    @Override
    public String getPathSeparator() {
        return DEFAULT_SEPARATOR;
    }

}
