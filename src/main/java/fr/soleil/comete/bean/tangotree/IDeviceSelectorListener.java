package fr.soleil.comete.bean.tangotree;

import java.util.List;

public interface IDeviceSelectorListener {

    public void selectionChanged(List<String> selection);
}
